from PIL import Image, ImageDraw

# Author: Adrian Antanyon
# Date: 12/03/2020
# Description: creacio de bandera
#

img = Image.new('RGB', (600, 400), 'White')

dib = ImageDraw.Draw(img)
dib.ellipse([150, 50, 449, 349], 'Crimson')
img.save('bandera.png')

